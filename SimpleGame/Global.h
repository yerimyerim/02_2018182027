#pragma once
#define MAX_OBJECTS 1000
#define GRAVITY 9.8
struct Inputs
{
	bool UP = false, DOWN = false, LEFT = false, RIGHT = false;
	bool ARROWUP = false, ARROWDOWN = false, ARROWLEFT = false, ARROWRIGHT = false;
	bool ANYEKEY = false, RESTARTKEY = false, EXITKEY = false, DASH = false;
};

struct UpdateParams
{
	GameVector3 force;
}; 
enum GameScene
{
	LOBBY,
	INGAME,
	ENDING
};
enum ObjectType
{
	HERO,
	MOVEABLE,
	MOVEDISABLE,
	BACKGROUND,
	BULLET,
	SWORD,
	MONSTER
};

enum ObjectState
{
	GROUND,	
	FALLING,
	FLY,
	SWIMMING,	
};