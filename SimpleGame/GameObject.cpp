#include "stdafx.h"
#include "GameObject.h"
#include <iostream>


GameObject::GameObject()
{
	Position = new GameVector3();
	Scale = new GameVector3();
	Accelator = new GameVector3();
	Velocity = new GameVector3();
	RelPosition = new GameVector3();
	Mass = -100.f;

	Parent = -1;
	LifeTime = 0.f;
	Life = 0.f;
	ApplyPhysics = false;
	CoolTime = 0.3f;
	RemainingCoolTime = CoolTime;
	StickToParent = false;
	TextureID = -1;
	State = ObjectState::FALLING;
	Type = ObjectType::MOVEABLE;
}

GameObject::~GameObject()
{
}

void GameObject::Update(float elapsedTimeInSec, UpdateParams* param)
{

	RemainingCoolTime -= elapsedTimeInSec;
	LifeTime -= elapsedTimeInSec;

	if (Type != ObjectType::MOVEABLE && Type != ObjectType::HERO && Type != ObjectType::MONSTER)
		return;

	if (!ApplyPhysics)
		return;

	if (StickToParent)
		return;

	float time = elapsedTimeInSec;
	float tt = time * time;

	float accelatorX = 0;
	float accelatorY = 0;

	//Update position
	if (Position->x < -11)
	{
		Velocity ->x = 0;
		Accelator->x = 0;
		Position->x = -11;
	}
	if (Position->x > 10)
	{
		Velocity->x = 0;
		Accelator->x = 0;
		Position->x = 10;
	}
	if (fabs(Velocity->x) > 0.f && State == ObjectState::GROUND)
	{

		accelatorX = param->force.x / Mass;

		accelatorX += Accelator->x;

		accelatorX += Velocity->x / fabs(Velocity->x) * 1.2 * (-GRAVITY);

		float tempVelx = Velocity->x + accelatorX * time;
			
		if (Velocity->x * tempVelx < 0.f)
		{
			Velocity->x = 0.f;
		}
			
		else
		{
			Velocity->x = tempVelx;
		}
			
		// 바닥 상태이기 때문에 더이상 가속도를 가지지않음.
		Velocity->y = 0.f;
	}
	else if (State == ObjectState::GROUND)
	{
		// 무한 가속중
		accelatorX = param->force.x / Mass;
		Velocity->x = Velocity->x + accelatorX * time;
	}
	if (State == ObjectState::GROUND)
	{
		accelatorY += Accelator->y;

		accelatorY += param->force.y / Mass;
	}
	else if (State == ObjectState::FALLING)
	{
		accelatorY -= GRAVITY;
	}

	//position : transform position 
	Velocity->y = Velocity->y + accelatorY * time;
		
	Position->x = Position->x + Velocity->x * time + 0.5 * accelatorX * tt;
	Position->y = Position->y + Velocity->y * time + 0.5 * accelatorY * tt;
	//std::cout << Position->x << " " << Position->y << std::endl;
}

void GameObject::SetPosition(GameVector3& position)
{
	Position->setVector3(position.getVector3());
}

void GameObject::SetScale(GameVector3& scale)
{
	Scale->setVector3(scale.getVector3());
}

void GameObject::GetPosition(float& x, float& y, float& z)
{
	x = Position->x;
	y = Position->y;
	z = Position->z;
}

void GameObject::GetScale(float& sx, float& sy, float& sz)
{
	sx = Scale->x;
	sy = Scale->y;
	sz = Scale->z;
}

void GameObject::SetVelocity(GameVector3& velocity)
{
	Velocity->setVector3(velocity.getVector3());
}

void GameObject::GetVelocity(float& x, float& y, float& z)
{
	x = Velocity->x;
	y = Velocity->y;
	z = Velocity->z;
}

GameVector3 GameObject::GetVelocity()
{
	return *this->Velocity;
}

void GameObject::SetAccelator(GameVector3& accelator)
{
	Accelator->setVector3(accelator.getVector3());
}

void GameObject::GetAccelator(float& x, float& y, float& z)
{
	x = Accelator->x;
	y = Accelator->y;
	z = Accelator->z;
}

GameVector3 GameObject::GetAccelator()
{
	return *this->Accelator;
}

void GameObject::SetMass(float& mass)
{
	Mass = mass;
}

void GameObject::GetMass(float& mass)
{
	mass = Mass;
}

float GameObject::GetMass()
{
	return Mass;
}

void GameObject::SetType(ObjectType type)
{
	Type = type;
}

void GameObject::GetType(ObjectType& type)
{
	type = Type;
}

ObjectType GameObject::GetType()
{
	return Type;
}


void GameObject::SetState(ObjectState state)
{
	State = state;
}

void GameObject::GetState(ObjectState& state)
{
	state = State;
}

ObjectState GameObject::GetState()
{
	return State;
}

void GameObject::SetTextureID(int id)
{
	TextureID = id;
}

int GameObject::GetTextureID()
{
	return TextureID;
}

GameVector3 GameObject::GetPosition()
{
	return *this->Position;
}

GameVector3 GameObject::GetScale()
{
	return *this->Scale;
}
void GameObject::SetParentID(int parentID)
{
	Parent = parentID;
}

void GameObject::SetLifeTime(float lifeTime)
{
	LifeTime = lifeTime;
}

void GameObject::SetLife(float life)
{
	Life = life;
}
void GameObject::SetRelPosition(float x, float y, float depth)
{
	RelPosition->x = x;
	RelPosition->y = y;
	RelPosition->z = depth;
}
void GameObject::SetApplyPhysics(bool bPhy)
{
	ApplyPhysics = bPhy;
}

void GameObject::SetCoolTime(float coolTime)
{
	CoolTime = coolTime;
}

void GameObject::ResetRemainingCoolTime()
{
	RemainingCoolTime = CoolTime;
}

void GameObject::SetStickToParent(bool bStick)
{
	if (Parent < 0)
	{
		std::cout << "This object has no parent" << std::endl;
		return;
	}

	if (StickToParent && !bStick)
	{
		Position->y = Position->y + RelPosition->y;
		Position->x = Position->x + RelPosition->x;
	}

	StickToParent = bStick;
}	

GameVector3 GameObject::GetRelPosition()
{
	return *RelPosition;
}
int GameObject::GetParentID()
{
	return Parent;
}

float GameObject::GetLife()
{
	return Life;
}

float GameObject::GetLifeTime()
{
	return LifeTime;
}

bool GameObject::GetApplyPhysics()
{
	return ApplyPhysics;
}

float GameObject::GetRemainingCoolTime()
{
	return RemainingCoolTime;
}

bool GameObject::GetStickToParent()
{
	return StickToParent;
}