#include "stdafx.h"
#include "GameManager.h"
#include "Dependencies/freeglut_std.h"


int temp = 0;
float testTime = 0.f;
float attacktime = 0;
int MoveCount[MAX_OBJECTS] = { 0 };
int idleTime = 0;
int shakeTime = 0;
GameManager::GameManager()
{
	//Renderer init
	int x = WINDOWSIZE_X;
	int y = WINDOWSIZE_Y;

	Render = new Renderer(x, y);
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		Objects[i] = NULL;
	}
	LobbySceneTexture = Render->GenPngTexture("./Dependencies/img/Lobby.png");
	EndingTexture = Render->GenPngTexture("./Dependencies/img/Ending.png");

	lightTexture = Render->GenPngTexture("./Dependencies/img/light.png");
	bgmSound = sound.CreateBGSound("./Dependencies/bgm/song_edm.ogg");
	lightParticle = Render->CreateParticleObject(
		100, -500, 0, 500, 500,
		10, 10, 20, 20,
		0, -5, 0, -10);

	LobbybgmSound = sound.CreateBGSound("./Dependencies/bgm/song_bunker_2.ogg");
	EndingbgmSound = sound.CreateBGSound("./Dependencies/bgm/song_ending.ogg");
	swordSound = sound.CreateShortSound("./Dependencies/bgm/sword.mp3");
	DeadSound = sound.CreateShortSound("./Dependencies/bgm/dead.wav");
	sound.PlayBGSound(LobbybgmSound, true, 0.1f);
}

void GameManager::GenerateInGame()
{
	HeroTexture = Render->GenPngTexture("./Dependencies/img/working_right.png");
	Player_Idle = Render->GenPngTexture("./Dependencies/img/idle_resize_player.png");
	Player_Attack_Right = Render->GenPngTexture("./Dependencies/img/attack_player_Right.png");
	Player_Attack_Left = Render->GenPngTexture("./Dependencies/img/attack_player_Left.png");
	Player_Walking_Right = Render->GenPngTexture("./Dependencies/img/working_right.png");
	Player_Walking_Left = Render->GenPngTexture("./Dependencies/img/working_left.png");
	Player_Roll = Render->GenPngTexture("./Dependencies/img/roll_Player.png");
	Player_dance = Render->GenPngTexture("./Dependencies/img/dance.png");
	BackGroundImg = Render->GenPngTexture("./Dependencies/img/map.png");
	SwordEffetLeft = Render->GenPngTexture("./Dependencies/img/slash_effect_Left.png");
	SwordEffetRight = Render->GenPngTexture("./Dependencies/img/slash_effect_Right.png");

	mapLight = Render->GenPngTexture("./Dependencies/img/map_light.png");

	mosterMovingTexture_Right = Render->GenPngTexture("./Dependencies/img/MonsterWalking_Right.png");
	mosterMovingTexture_Left = Render->GenPngTexture("./Dependencies/img/MonsterWalking_Left.png");

	mosterAttackTexture_Right = Render->GenPngTexture("./Dependencies/img/MonsterAttack_Right.png");
	mosterAttackTexture_Left = Render->GenPngTexture("./Dependencies/img/MonsterAttack_Left.png");
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		Objects[i] = NULL;
	}

	resetGameScene();
}

void GameManager::resetGameScene()
{

	float mass = 20.f;
	HeroID = AddObject(-3.0, -4.5, 1, 1, 1, GameVector3(0, 0, 0), GameVector3(0, 0, 0), mass);
	Objects[HeroID]->SetType(ObjectType::HERO);
	Objects[HeroID]->SetApplyPhysics(true);
	Objects[HeroID]->SetLife(100000000.f);
	Objects[HeroID]->SetLifeTime(100000000.f);
	Objects[HeroID]->SetTextureID(Player_Idle);
	Objects[HeroID]->AnimateFrame = 11;
	Objects[HeroID]->margin = 100;
	Render->SetCameraPos(-5.0 * 100, -6.0*100);
	//std::cout << "ũ�� " << Objects[HeroID]->GetScale().x << std::endl;
	mass = 10.f;
	int floor = AddObject(0, -4.3 - 6, 0, 24, 2, GameVector3(0, 0, 0), GameVector3(0, 0, 0), (float)mass);
	Objects[floor]->SetType(ObjectType::MOVEDISABLE);
	Objects[floor]->SetApplyPhysics(true);
	Objects[floor]->SetLife(100000000.f);
	Objects[floor]->SetLifeTime(100000000.f);
	//Objects[floor]->SetTextureID(BrickTexture);

	floor = AddObject(-1, -5.5, 0, 16, 0.2, GameVector3(0, 0, 0), GameVector3(0, 0, 0), (float)mass);
	Objects[floor]->SetType(ObjectType::MOVEDISABLE);
	Objects[floor]->SetApplyPhysics(true);
	Objects[floor]->SetLife(100000000.f);
	Objects[floor]->SetLifeTime(100000000.f);

	for (int i = 0; i < 23; ++i)
	{
		floor = AddObject(6.5 + i * 0.2, -4.3 + i * 0.2 - 5, 0, 0, 0.3, GameVector3(0, 0, 0), GameVector3(0, 0, 0), (float)mass);
		Objects[floor]->SetType(ObjectType::MOVEDISABLE);
		Objects[floor]->SetApplyPhysics(true);
		Objects[floor]->SetLife(100000000.f);
		Objects[floor]->SetLifeTime(100000000.f);
	}

	MonsterCount = 0;


	mosterID = AddObject(0, -1.5, 1, 1, 1, GameVector3(0, 0, 0), GameVector3(0, 0, 0), mass);
	Objects[mosterID]->SetType(ObjectType::MOVEABLE);
	Objects[mosterID]->SetApplyPhysics(true);
	Objects[mosterID]->SetLife(100000000.f);
	Objects[mosterID]->SetLifeTime(100000000.f);
	Objects[mosterID]->SetTextureID(mosterMovingTexture_Right);
	Objects[mosterID]->AnimateFrame = 10;
	Objects[mosterID]->margin = 100;
	Objects[mosterID]->IsMoveRight = false;
	++MonsterCount;

	mosterID = AddObject(3, -1.5, 1, 1, 1, GameVector3(0, 0, 0), GameVector3(0, 0, 0), mass);
	Objects[mosterID]->SetType(ObjectType::MOVEABLE);
	Objects[mosterID]->SetApplyPhysics(true);
	Objects[mosterID]->SetLife(100000000.f);
	Objects[mosterID]->SetLifeTime(100000000.f);
	Objects[mosterID]->SetTextureID(mosterMovingTexture_Right);
	Objects[mosterID]->AnimateFrame = 10;
	Objects[mosterID]->margin = 100;
	++MonsterCount;

	mosterID = AddObject(-2, -1.5, 1, 1, 1, GameVector3(0, 0, 0), GameVector3(0, 0, 0), mass);
	Objects[mosterID]->SetType(ObjectType::MOVEABLE);
	Objects[mosterID]->SetApplyPhysics(true);
	Objects[mosterID]->SetLife(100000000.f);
	Objects[mosterID]->SetLifeTime(100000000.f);
	Objects[mosterID]->SetTextureID(mosterMovingTexture_Right);
	Objects[mosterID]->AnimateFrame = 10;
	Objects[mosterID]->margin = 100;
	Objects[mosterID]->IsMoveRight = true;
	++MonsterCount;

	sound.PlayBGSound(bgmSound, true, 0.1f);
}

bool GameManager::AABBColision(GameObject* a, GameObject* b)
{
	ObjectType aType = a->GetType();
	ObjectType bType = b->GetType();;

	GameVector3 aPosition = a->GetPosition();
	GameVector3 bPosition = b->GetPosition();
	GameVector3 aSize = a->GetScale();
	GameVector3 bSize = b->GetScale();

	GameVector3 aMin = GameVector3(aPosition.x - aSize.x * 0.5, aPosition.y - aSize.y * 0.5, 0);
	GameVector3 aMax = GameVector3(aPosition.x + aSize.x * 0.5, aPosition.y + aSize.y * 0.5, 0);
	GameVector3 bMin = GameVector3(bPosition.x - bSize.x * 0.5, bPosition.y - bSize.y * 0.5, 0);
	GameVector3 bMax = GameVector3(bPosition.x + bSize.x * 0.5, bPosition.y + bSize.y * 0.5, 0);
	
	if (aMin.x > bMax.x)
	{
		return false;
	}
	if (aMax.x < bMin.x)
	{
		return false;
	}
	if (aMin.y > bMax.y)
	{
		return false;
	}
	if (aMax.y < bMin.y)
	{
		return false;
	}
	AdjustPosition(a, b);
	return true;

}

bool GameManager::ProcessCollision(GameObject* a, GameObject* b)
{

	ObjectType aType = a->GetType();
	ObjectType bType = b->GetType();
	bool isCollide = AABBColision(a, b);
	if (isCollide)
	{
		// do somthing;
		if (aType == ObjectType::MOVEDISABLE || bType == ObjectType:: MOVEDISABLE)
		{
			a->SetState(ObjectState::GROUND);
			b->SetState(ObjectState::GROUND);
		}
	}
	return isCollide;
}

void GameManager::AdjustPosition(GameObject* a, GameObject* b)
{
	ObjectType aType = a->GetType();
	ObjectType bType = b->GetType();;

	GameVector3 aPosition = a->GetPosition();
	GameVector3 bPosition = b->GetPosition();
	GameVector3 aSize = a->GetScale();
	GameVector3 bSize = b->GetScale();

	GameVector3 aMin = GameVector3(aPosition.x - aSize.x * 0.5, aPosition.y - aSize.y * 0.5, 0);
	GameVector3 aMax = GameVector3(aPosition.x + aSize.x * 0.5, aPosition.y + aSize.y * 0.5, 0);
	GameVector3 bMin = GameVector3(bPosition.x - bSize.x * 0.5, bPosition.y - bSize.y * 0.5, 0);
	GameVector3 bMax = GameVector3(bPosition.x + bSize.x * 0.5, bPosition.y + bSize.y * 0.5, 0);

	if ((aType == ObjectType::MOVEABLE || aType == ObjectType::HERO || aType == ObjectType::MONSTER) && bType == ObjectType::MOVEDISABLE)
	{
		if (aMax.y > bMax.y)
		{
			aPosition.y = aPosition.y + (bMax.y - aMin.y);
			
			a->SetPosition(aPosition);
			a->SetVelocity(GameVector3(a->GetVelocity().x, 0.f, 0.f));
		}
		else
		{
			aPosition.y = aPosition.y - (aMax.y - bMin.y);
			a->SetPosition(aPosition);
			a->SetVelocity(GameVector3(a->GetVelocity().x, 0.f, 0.f));
		}

	}
	else if ((bType == ObjectType::MOVEABLE || bType == ObjectType::HERO || aType == ObjectType::MONSTER) && aType == ObjectType::MOVEDISABLE)
	{
		if (!(bMax.y > aMax.y && bMin.y < aMin.y))
		{
			if (bMax.y > aMax.y)
			{
				bPosition.y = bPosition.y + (aMax.y - bMin.y);
				b->SetPosition(bPosition);
				b->SetVelocity(GameVector3(b->GetVelocity().x, 0.f, 0.f));
			}
			else
			{
				bPosition.y = bPosition.y - (bMax.y - aMin.y);
				b->SetPosition(bPosition);
				b->SetVelocity(GameVector3(b->GetVelocity().x, 0.f, 0.f));
			}
		}
	}
}

int GameManager::AddObject(float x, float y, float z, float sx , float sy , GameVector3& velocity, GameVector3& accelator , float& mass )
{
	int index = 0;
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}
	if (index < 0)
	{
		std::cout << "Empty Object Slot" << std::endl;
		return -1;
	}

	Objects[index] = new GameObject();
	Objects[index]->SetPosition(GameVector3( x, y, z));
	Objects[index]->SetScale(GameVector3(sx, sy, 0));
	Objects[index]->SetVelocity(velocity);
	Objects[index]->SetAccelator(accelator);
	Objects[index]->SetMass(mass);	
	return index;
}

void GameManager::DeleteObject(int index)
{
	//find empty slot
	if (Objects[index] != NULL)
	{
		delete Objects[index];
		Objects[index] = NULL;
	}
	else
	{
		std::cout << "Try to delete NULL obj" << index << std::endl;
	}
}


GameManager::~GameManager()
{
	//Renderer d
}

void GameManager::Update(float elapsedTimeInSec, Inputs* inputs)
{
		//	std::cout << 1.f / elapsedTimeInSec << std::endl;
		DoGarbageCollect();
		switch (gameScene)
		{
		case LOBBY:
			if (inputs->ANYEKEY && !inputs->RESTARTKEY)
			{
				GenerateInGame();
				sound.StopBGSound(LobbybgmSound);
				gameScene = INGAME;
			}
			break;
		case INGAME:
			InGameSceneUpdate(inputs, elapsedTimeInSec);
			if (MonsterCount == 0)
			{
				Render->SetCameraPos(0,0);
				gameScene = ENDING;
				for (int i = 0; i < MAX_OBJECTS; ++i)
				{
					Objects[i] = NULL;
				}
				sound.StopBGSound(bgmSound);
				sound.PlayBGSound(EndingbgmSound, true, 0.1f);
			}
			break;
		case ENDING:

			if (inputs->RESTARTKEY)
			{
				sound.StopBGSound(EndingbgmSound);
				sound.PlayBGSound(LobbybgmSound, true, 0.1f);
				gameScene = LOBBY;
			}
			break;
		default:
			break;
		}
		//cout << Render->m_v3Camera_Position.x << endl;
}

void GameManager::InGameSceneUpdate(Inputs* inputs, float elapsedTimeInSec)
{
	UpdateParams othresParam;
	UpdateParams heroParam;
	UpdateParams mosterParam[100];
	memset(&othresParam, 0, sizeof(othresParam));
	memset(&heroParam, 0, sizeof(heroParam));
	memset(&mosterParam, 0, sizeof(mosterParam));

	float forceAmount = 300.0f;

	//update position
	if (idleTime < 100)
	{
		Objects[HeroID]->SetTextureID(Player_Idle);
		Objects[HeroID]->AnimateFrame = 11;
		++idleTime;
	}
	else
	{
		Objects[HeroID]->SetTextureID(Player_dance);
		Objects[HeroID]->AnimateFrame = 12;
	}
	//cout << Objects[HeroID]->GetPosition().x << endl;
	if (inputs->UP) {
		if (inputs->DASH)
		{
			heroParam.force.y += forceAmount;
		}
		heroParam.force.y += 20 * forceAmount; 
		idleTime = 0;
	}
	if (inputs->DOWN) { 
		if (inputs->DASH)
		{
			heroParam.force.y -= forceAmount;
		}
		heroParam.force.y -= forceAmount; 
		idleTime = 0;
	}
	if (inputs->RIGHT)
	{
		if (inputs->DASH)
		{
			heroParam.force.x += forceAmount;
		}
		heroParam.force.x += forceAmount;
		Objects[HeroID]->SetTextureID(Player_Walking_Right);
		Objects[HeroID]->AnimateFrame = 10;
		idleTime = 0;
	}
	if (inputs->LEFT)
	{

		if (inputs->DASH)
		{
			heroParam.force.x -= forceAmount;
		}
		heroParam.force.x -= forceAmount;
		Objects[HeroID]->SetTextureID(Player_Walking_Left);
		Objects[HeroID]->AnimateFrame = 10;
		idleTime = 0;
	}
	//sword
	float swordPosX = 0.f;
	float swordPosY = 0.f;

	if (inputs->ARROWLEFT)
	{
		swordPosX += -1.f;
		Objects[HeroID]->SetTextureID(Player_Attack_Left);
		Objects[HeroID]->AnimateFrame = 7;
		idleTime = 0;
	};
	if (inputs->ARROWRIGHT)
	{
		swordPosX += 1.f;
		Objects[HeroID]->SetTextureID(Player_Attack_Right);
		Objects[HeroID]->AnimateFrame = 7;
		idleTime = 0;
	};
	if (inputs->ARROWDOWN)
	{
		swordPosY += -1.f;
		idleTime = 0;
	}
	if (inputs->ARROWUP)
	{
		swordPosY += 1.f;
		Objects[HeroID]->SetTextureID(Player_Attack_Right);
		Objects[HeroID]->AnimateFrame = 7;
		idleTime = 0;
	}

	float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);

	if (swordDirSize > 0.f)
	{
		float norDirX = swordPosX / swordDirSize;
		float norDirY = swordPosY / swordDirSize;


		GameVector3* HeroPosition = new GameVector3(Objects[HeroID]->GetPosition());
		GameVector3* HeroSize = new GameVector3(Objects[HeroID]->GetScale());


		if (Objects[HeroID]->GetRemainingCoolTime() < 0.f)
		{
			float mass = 0.f;
			sound.PlayShortSound(swordSound, false, 0.2f);
			swordID = AddObject(0.f, 0.f, 0.f, 2.f, 2.f, GameVector3(0.f, 0.f, 0.f), GameVector3(0.f, 0.f, 0.f), mass);
			Objects[swordID]->SetParentID(HeroID);
			Objects[swordID]->SetRelPosition(norDirX, norDirY, 0.f);
			Objects[swordID]->SetStickToParent(true);
			Objects[swordID]->SetLife(100.f);
			Objects[swordID]->SetLifeTime(0.3f);
			Objects[swordID]->SetType(ObjectType::SWORD);
			if (inputs->ARROWRIGHT)
			{
				Objects[swordID]->SetTextureID(SwordEffetRight);
				Objects[swordID]->Ismoving = true;
				Objects[swordID]->AnimateFrame = 4;
				Objects[swordID]->margin = 0;
			}
			if (inputs->ARROWLEFT)
			{
				Objects[swordID]->SetTextureID(SwordEffetLeft);
				Objects[swordID]->Ismoving = true;
				Objects[swordID]->AnimateFrame = 4;
				Objects[swordID]->margin = 0;
			}
			if (inputs->ARROWUP)
			{
				Objects[swordID]->SetTextureID(SwordEffetRight);
				Objects[swordID]->Ismoving = true;
				Objects[swordID]->AnimateFrame = 4;
				Objects[swordID]->margin = 0;
			}
		}
	}
	// processing colision
	bool isCollide[MAX_OBJECTS];
	memset(isCollide, 0, sizeof(bool) * MAX_OBJECTS);
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		for (int j = i + 1; j < MAX_OBJECTS; ++j)
		{
			if (Objects[i] != NULL && Objects[j] != NULL)
			{
				if (Objects[i]->GetApplyPhysics() && Objects[j]->GetApplyPhysics()
					&& Objects[j]->GetType() != ObjectType::BACKGROUND && Objects[i]->GetType() != ObjectType::BACKGROUND)
				{
					bool collide = ProcessCollision(Objects[i], Objects[j]);
					if (collide)
					{
						isCollide[i] = true;
						isCollide[j] = true;
					}
				}
				if (Objects[j]->GetType() == ObjectType::SWORD && Objects[i]->GetType() == ObjectType::MOVEABLE)
				{
					bool collide = ProcessCollision(Objects[i], Objects[j]);
					if (collide)
					{
						++shakeTime;
						sound.PlayShortSound(DeadSound, false, 0.1f);
						DeleteObject(i);
						--MonsterCount;
					}
				}
				if (Objects[i]  != NULL&&Objects[i]->GetType() == ObjectType::HERO && Objects[j]->GetType() == ObjectType::MOVEABLE)
				{
					bool collide = ProcessCollision(Objects[i], Objects[j]);
					if (collide)
					{
						if (Objects[j]->IsAttacking && attacktime > 1)
						{
							for (int k = 0; k < MAX_OBJECTS; ++k)
							{
								Objects[k] = NULL;
							}
							attacktime = 0;
							sound.PlayShortSound(DeadSound, false, 0.1f);
							resetGameScene();
						}
						if (0 < Objects[j]->GetPosition().x - Objects[HeroID]->GetPosition().x )
						{
							Objects[j]->SetTextureID(mosterAttackTexture_Left);
							Objects[j]->AnimateFrame = 8;
							Objects[j]->IsAttacking = true;
							attacktime += elapsedTimeInSec;
						}						
						if (0 > Objects[j]->GetPosition().x - Objects[HeroID]->GetPosition().x)
						{
							Objects[j]->SetTextureID(mosterAttackTexture_Right);
							Objects[j]->AnimateFrame = 8;
							Objects[j]->IsAttacking = true;
							attacktime += elapsedTimeInSec;
						}
					}
					else
					{
						Objects[j]->IsAttacking = false;
					}
				}
			}
		}
	}
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (Objects[i] != NULL)
		{
			if (!isCollide[i])
				Objects[i]->SetState(ObjectState::FALLING);
		}
	}
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (Objects[i] != NULL)
		{
			if (i == HeroID)
			{
				Objects[i]->Update(elapsedTimeInSec, &heroParam);
			}
			else
			{
				if (Objects[i]->GetStickToParent())
				{
					int parentID = Objects[i]->GetParentID();
					GameVector3* pos = new GameVector3(Objects[parentID]->GetPosition());
					GameVector3* relpos = new GameVector3(Objects[i]->GetRelPosition());
					pos->x += relpos->x;
					pos->y += relpos->y;
					pos->z += relpos->z;

					Objects[i]->SetPosition(*pos);
					Objects[i]->Update(elapsedTimeInSec, &othresParam);
				}
				else
				{
					Objects[i]->Update(elapsedTimeInSec, &othresParam);
				}
			}
		}
	}
	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (Objects[i] != NULL)
		{
			if (Objects[i]->GetType() == MOVEABLE && !Objects[i]->IsAttacking)
			{
				int distance = (Objects[HeroID]->GetPosition().x - Objects[i]->GetPosition().x) * (Objects[HeroID]->GetPosition().x - Objects[i]->GetPosition().x)
					+ (Objects[HeroID]->GetPosition().y - Objects[i]->GetPosition().y) * (Objects[HeroID]->GetPosition().y - Objects[i]->GetPosition().y);

				if (0 < Objects[i]->GetPosition().x - Objects[HeroID]->GetPosition().x && distance < 9)
				{
					mosterParam[i].force.x -= forceAmount*0.8;
					Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					Objects[i]->SetTextureID(mosterMovingTexture_Left);
					Objects[i]->AnimateFrame = 10;
					Objects[i]->IsAttacking = true;
				}
				else if (0 > Objects[i]->GetPosition().x - Objects[HeroID]->GetPosition().x && distance < 9)
				{
					mosterParam[i].force.x += forceAmount * 0.8;
					Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					Objects[i]->SetTextureID(mosterMovingTexture_Right);
					Objects[i]->AnimateFrame = 10;
					Objects[i]->IsAttacking = true;
				}
				else
				{	
					if (MoveCount[i] > 30)
					{
						int moveDirection = rand() % 2;
						if (moveDirection == 1)
						{
							MoveCount[i] = 0;
							Objects[i]->IsMoveRight = true;
						}
						else
						{
							MoveCount[i] = 0;
							Objects[i]->IsMoveRight = false;
						}
					}
					if (Objects[i]->IsMoveRight)
					{
						mosterParam[i].force.x += forceAmount * 0.8;
						Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
						Objects[i]->SetTextureID(mosterMovingTexture_Right);
						Objects[i]->AnimateFrame = 10;
						Objects[i]->IsAttacking = true;
						MoveCount[i] += 1;
					}
					else
					{
						mosterParam[i].force.x -= forceAmount * 0.8;
						Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
						Objects[i]->SetTextureID(mosterMovingTexture_Left);
						Objects[i]->AnimateFrame = 10;
						Objects[i]->IsAttacking = true;
						MoveCount[i] += 1;
					}
					//int x = rand() % 2;
					//if (x == 1 && mosterParam[i].force.z >= elapsedTimeInSec * 10)
					//{
					//	mosterParam[i].force.x += forceAmount * 0.6;
					//	Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					//	Objects[i]->SetTextureID(mosterMovingTexture_Right);
					//	Objects[i]->AnimateFrame = 10;
					//	mosterParam[i].force.z = 0;
					//}
					//else if (x == 0 && mosterParam[i].force.z >= elapsedTimeInSec * 10)
					//{
					//	mosterParam[i].force.z = 0;
					//	mosterParam[i].force.x -= forceAmount * 0.6;
					//	Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					//	Objects[i]->SetTextureID(mosterMovingTexture_Left);
					//	Objects[i]->AnimateFrame = 10;
					//}
					//else
					//{
					//	if (mosterParam[i].force.x < 0)
					//	{
					//		mosterParam[i].force.x -= forceAmount * 0.6;
					//		Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					//		Objects[i]->SetTextureID(mosterMovingTexture_Left);
					//		Objects[i]->AnimateFrame = 10;
					//		++mosterParam[i].force.z;
					//	}
					//	else
					//	{
					//		mosterParam[i].force.x += forceAmount * 0.6;
					//		Objects[i]->Update(elapsedTimeInSec, &mosterParam[i]);
					//		Objects[i]->SetTextureID(mosterMovingTexture_Right);
					//		Objects[i]->AnimateFrame = 10;
					//		++mosterParam[i].force.z;
					//	}
					//}
				}
			}
		}
	}
	if (shakeTime != 0 && shakeTime < 20)
	{
		CameraShakeMove();
		++shakeTime;
	}
	else if (shakeTime > 20)
	{
		shakeTime = 0;
	}
	else
	{
		CameraMove();
	}
}

void GameManager::CameraMove()
{
	if (Objects[HeroID]->GetPosition().x < -6 || Objects[HeroID]->GetPosition().x > 6) {}
	else { Render->SetCameraPosX(Objects[HeroID]->GetPosition().x * 100); }
	if (Objects[HeroID]->GetPosition().y < -6 || Objects[HeroID]->GetPosition().y > -4.5) {}
	else { Render->SetCameraPosY(Objects[HeroID]->GetPosition().y * 100); }
}
void GameManager::CameraShakeMove()
{
	if (Objects[HeroID]->GetPosition().x < -6 || Objects[HeroID]->GetPosition().x > 6) {}
	else { Render->SetCameraPosX(Objects[HeroID]->GetPosition().x * 100 + 5 - rand() % 10); }
	if (Objects[HeroID]->GetPosition().y < -6 || Objects[HeroID]->GetPosition().y > -4.5) {}
	else { Render->SetCameraPosY(Objects[HeroID]->GetPosition().y * 100 + 5 - rand() % 10); }
}

void GameManager::RenderScene()
{
	//cout << Objects[HeroID]->GetPosition().y<<endl;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);
	switch (gameScene)
	{
	case LOBBY:
			Render->DrawGround(0, 0, -1, 1100, 800, 1, 1, 1, 1, 1, LobbySceneTexture);
		break;
	case INGAME:
			InGameRenderScene();
			//Render->DrawGround(0, 0, -1, 1100, 800, 1, 1, 1, 1, 1, mapLight);
		break;
	case ENDING:
			Render->DrawGround(0, 0, -1, 1100, 800, 1, 1, 1, 1, 1, EndingTexture);
			Render->DrawParticle(lightParticle,
				0, 0, 0,
				1.f,
				1, 1, 1, 1,
				0, 0,
				lightTexture,
				1.f,
				testTime);
			testTime += 0.016;
		break;
	default:
		break;
	}
}
void GameManager::InGameRenderScene()
{
	Render->DrawGround(0, -500, -1, 2400, 1000, 1, 1, 1, 1, 1, BackGroundImg);
	if (idleTime >= 100)
	{
		Render->DrawGround(0, -500, -1, 2400, 1000, 1, rand() % 100 * 0.01f, rand() % 100 * 0.01f, rand() % 100 * 0.01f, 1 , mapLight);
	}
	++temp;

	for (int i = 0; i < MAX_OBJECTS; ++i)
	{
		if (Objects[i] != NULL)
		{
			float x, y, z;
			Objects[i]->GetPosition(x, y, z);
			float sx, sy, sz;
			Objects[i]->GetScale(sx, sy, sz);
			int textureID = Objects[i]->GetTextureID();
			x = x * 100.f;
			y = y * 100.f;
			sy = sy * 100.f;
			sx = sx * 100.f;
			if (textureID < 0)
			{
				//Render->DrawSolidRect(x, y, z, sx, sy, 0.f, 1, 0, 1, 1, true);
			}
			else
			{
				if (Objects[i]->Ismoving)
				{
					int f = Objects[i]->AnimateFrame;
					int t = floor(temp / f);
					//cout << t << endl;
					//Render->DrawSolidRect(x, y, z, sx, sy, 0.f, 1, 0, 1, 1, true);
					if (Objects[HeroID]->GetTextureID() == Player_dance)
					{
						Render->DrawTextureRectAnim(x, y, -1, sx, sy, sz, rand() % 100 * 0.01f, rand() % 100 * 0.01f, rand() % 100 * 0.01f, 1, textureID, f, 1, t*2 % f, 0, Objects[i]->margin, true);
					}
					else
						Render->DrawTextureRectAnim(x, y, -1, sx, sy, sz, 1,1,1, 1, textureID, f, 1, t % f, 0, Objects[i]->margin, true);
				}
				else
				{
					Render->DrawTextureRect(x, y, z, sx, sy, 1.f, 1.f, 1.f, 1.f, 1.f, textureID);
				}
			}
		}
	}

}
void GameManager::DoGarbageCollect()
{
	for (int i = 0; i < MAX_OBJECTS; i++)
	{
		if (Objects[i] != NULL)
		{
			float life = Objects[i]->GetLife();
			float lifeTime = Objects[i]->GetLifeTime();
			if (life < 0.f || lifeTime < 0.f)
			{
				DeleteObject(i);
			}
		}
	}
}