#pragma once
#include "GameVector3.h"
#include "Global.h"

class GameObject
{
public:
	GameObject();
	~GameObject();
	void Update(float elapsedTimeInSec, UpdateParams* param );

	void SetPosition(GameVector3& position);
	void GetPosition(float& x, float& y, float& z);
	void GetScale(float& sx, float& sy, float& sz);
	GameVector3 GetPosition();
	void SetScale(GameVector3& scale);
	GameVector3 GetScale();

	void SetParentID(int parentID);

	void SetLifeTime(float lifeTime);

	void SetLife(float life);

	void SetRelPosition(float x, float y, float depth);

	void SetApplyPhysics(bool bPhy);

	void SetCoolTime(float coolTime);

	void ResetRemainingCoolTime();

	void SetStickToParent(bool bStick);

	GameVector3 GetRelPosition();

	int GetParentID();

	float GetLife();

	
	float GetLifeTime();

	bool GetApplyPhysics();

	
	float GetRemainingCoolTime();

	bool GetStickToParent();
	
	void SetVelocity(GameVector3& velocity);
	void GetVelocity(float& x, float& y, float& z);
	GameVector3 GetVelocity();

	void SetAccelator(GameVector3& accelator);
	void GetAccelator(float& x, float& y, float& z);
	GameVector3 GetAccelator();

	void SetMass(float& mass);
	void GetMass(float& mass);
	float GetMass();

	void SetType(ObjectType type);
	void GetType(ObjectType &type);
	ObjectType GetType();	
	
	void SetState(ObjectState state);
	void GetState(ObjectState &state);
	ObjectState GetState();

	void SetTextureID(int id);
	int GetTextureID();
	bool Ismoving = true;
	int AnimateFrame = 0;
	int margin = -1;
	bool IsMoveRight = false;
	bool IsAttacking = false;
private:
	GameVector3* Position;
	GameVector3* Scale;
	GameVector3* Velocity;
	GameVector3* Accelator;
	GameVector3* RelPosition;
	float Mass;

	int Parent;

	float LifeTime;
	float Life;
	float CoolTime;  
	float RemainingCoolTime;	
	bool ApplyPhysics;
	bool StickToParent;
	int TextureID;
	ObjectType Type;
	ObjectState State;
};

