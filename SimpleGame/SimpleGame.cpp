/*
Copyright 2020 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "GameManager.h"
int winid;


GameManager* gameManager = NULL;
int g_prevTimeInMillisecond = 0;
Inputs inputs;
bool g_focused = true;

void RenderScene(int temp)
{
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	int elapsedTime = currentTime - g_prevTimeInMillisecond;
	g_prevTimeInMillisecond = currentTime;
	float elapsedTimeInSec = (float)elapsedTime / 1000.f;

	Inputs tempInputs;
	memcpy(&tempInputs, &inputs, sizeof(Inputs));

	if (g_focused)
	{
		gameManager->Update(elapsedTimeInSec, &tempInputs);
		gameManager->RenderScene();
	}

	glutSwapBuffers(); //double buffering, front->back, back->front

	glutTimerFunc(16, RenderScene, 16);
}
void Display(void)
{
	g_focused = true;
}
void Idle(void)
{
}
void Reshape(int width, int height)
{
	g_focused = false;
}
void MouseInput(int button, int state, int x, int y)
{
}

void KeyDownInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w' | 'W':
		inputs.UP = true;
		break;
	case 'a' | 'A':
		inputs.LEFT = true;
		break;
	case 's' | 'S':
		inputs.DOWN = true;
		break;
	case 'd' | 'D':
		inputs.RIGHT = true;
		break;
	case 'r':
		inputs.RESTARTKEY = true;
		break;
	case 32:
		inputs.DASH = true;
		break;
	case 27:
	{
		glutDestroyWindow(winid);
	}break;
	default:
	{
		inputs.ANYEKEY = true;
	}break;
	}
}

void KeyUpInput(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'w' | 'W':
		inputs.UP = false;
		break;
	case 'a' | 'A':
		inputs.LEFT = false;
		break;
	case 's' | 'S':
		inputs.DOWN = false;
		break;
	case 'd' | 'D':
		inputs.RIGHT = false;
		break;
	case 'r':
		inputs.RESTARTKEY = false;
		break;
	case 32:
		inputs.DASH = false;
		
	default:
	{
		inputs.ANYEKEY = false;
	}
		break;
	}
}

void SpecialKeyDownInput(int key, int x, int y)
{
	switch (key)
	{
	case  GLUT_KEY_UP:
	{
		inputs.ARROWUP = true;
	}break;
	case  GLUT_KEY_DOWN:
	{
		inputs.ARROWDOWN = true;
	}break;
	case  GLUT_KEY_RIGHT:
	{
		inputs.ARROWRIGHT = true;
	}break;
	case  GLUT_KEY_LEFT:
	{
		inputs.ARROWLEFT = true;
	}break;

	default:
	{
		inputs.ANYEKEY = true;
	}
	}
}

void SpecialKeyUpInput(int key, int x, int y)
{
	switch (key)
	{
	case  GLUT_KEY_UP:
	{
		inputs.ARROWUP = false;
	}break;
	case  GLUT_KEY_DOWN:
	{
		inputs.ARROWDOWN = false;
	}break;
	case  GLUT_KEY_RIGHT:
	{
		inputs.ARROWRIGHT = false;
	}break;
	case  GLUT_KEY_LEFT:
	{
		inputs.ARROWLEFT = false;
	}break;
	default: {
		inputs.ANYEKEY = false;
	}break;
	}
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	int x = WINDOWSIZE_X;
	int y = WINDOWSIZE_Y;
	glutInitWindowSize(x, y);
	winid = glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}
	gameManager = new GameManager();

	memset(&inputs, 0, sizeof(inputs));

	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_prevTimeInMillisecond = glutGet(GLUT_ELAPSED_TIME);

	glutTimerFunc(16, RenderScene, 16);

	glutMainLoop();

	delete gameManager;

    return 0;
}

