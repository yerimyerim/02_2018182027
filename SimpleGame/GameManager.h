#pragma once
#include "Sound.h"
#include "Renderer.h"
#include "GameObject.h"
#define WINDOWSIZE_X 1024;
#define WINDOWSIZE_Y 800;

class GameManager
{
private:
	bool AABBColision(GameObject *a, GameObject *b );
	bool ProcessCollision(GameObject* a, GameObject* b);
	void AdjustPosition(GameObject* a, GameObject* b);
	Renderer* Render;
	GameObject* Objects[MAX_OBJECTS];
	int AddObject(float x, float y, float z, float sx, float sy, GameVector3& velocity, GameVector3& accelator, float& mass);
	void DeleteObject(int index);
	int HeroID = -1;
	int mosterID = -1;
	int HeroTexture = -1;
	int BrickTexture = -1;
	int SpriteSheet = -1;
	int swordID = -1;

	int Player_Walking_Right = -1;
	int Player_Walking_Left = -1;
	int Player_Idle = -1;
	int Player_Attack_Right = -1;
	int Player_Attack_Left = -1;
	int Player_Roll = -1;

	int BackGroundImg = -1;
	int bgmSound = -1;
	
	int SwordEffetRight = -1;
	int SwordEffetLeft = -1;

	int mosterMovingTexture_Right = -1;
	int mosterMovingTexture_Left = -1;
	
	int LobbySceneTexture = -1;
	int EndingTexture = -1;

	int mosterAttackTexture_Right = -1;
	int mosterAttackTexture_Left = -1;

	int lightTexture = -1;
	int lightParticle = -1;
	
	int MonsterCount = 0;
	
	bool GameEnd = false;

	int LobbybgmSound = -1;
	int EndingbgmSound = -1;

	int swordSound = -1;
	int Player_dance = -1;
	int mapLight = -1;
	Sound sound;
	GameScene gameScene = LOBBY;

	int DeadSound = -1;
public:
	GameManager();
	void GenerateInGame();
	void resetGameScene();
	GameManager(int width, int height);
	~GameManager();
	void Update(float elapsedTimeInSec, Inputs *inputs);
	void InGameSceneUpdate(Inputs* inputs, float elapsedTimeInSec);
	void CameraMove();
	void CameraShakeMove();
	void RenderScene();
	void InGameRenderScene();
	void DoGarbageCollect();

};

