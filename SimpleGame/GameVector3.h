#pragma once
class GameVector3
{
public:
	float x;
	float y;
	float z;
	GameVector3(float dx , float dy , float dz)
	{
		x = dx;
		y = dy;
		z = dz;
	}
	GameVector3() 
	{
		x = -100;
		y = -100;
		z = -100;
	}
	void setVector3(float dx, float dy, float dz)
	{
		x = dx;
		y = dy;
		z = dz;
	}
	void setVector3(GameVector3 vector)
	{
		*this = vector;
	}
	GameVector3 getVector3()
	{
		return *this;
	}
};

